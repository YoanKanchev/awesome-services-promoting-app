function Rank() {
    return (
        <div>
            <div className={'center white f3'}>
                {'...your current rank is...'}
            </div>
            <div className={'center white f1'}>
                {'#...'}
            </div>
        </div>

    )
}

export default Rank;