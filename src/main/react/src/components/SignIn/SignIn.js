import * as Constants from '../../Constants';
import React from "react";
import {types, withAlert} from "react-alert";
import * as Yup from 'yup';
import {Formik} from 'formik'

const SignInForm = ({onRouteChange, loadUser, alert}) => (
    <Formik
        initialValues={{email: "", password: ""}}
        onSubmit={(values, {setSubmitting}) => {
            fetch("/authenticate",
                {
                    method: "post",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                            email: values.email,
                            password: values.password
                        }
                    )
                })
                .then(response => response.json())
                .then(user => {
                    debugger
                    if (!user.error) {
                        loadUser(user);
                        onRouteChange(Constants.HOME_ROUTE);
                        setSubmitting(true);
                        debugger
                    } else {
                        alert.show(user.message, {type: types.ERROR});
                        setSubmitting(false);
                    }
                })
                .catch(err => {
                        alert.show(err)
                    }
                )
        }}
        //********Using Yum for validation********/

        validationSchema={Yup.object().shape({
            email: Yup.string()
                .email("Must be a valid email")
                .required("Required!"),
            password: Yup.string()
                .required("Required!")
            // .min(8, "Password is too short - should be 8 chars minimum.")
            // .matches(/(?=.*[0-9])/, "Password must contain a number.")
        })}
    >
        {(props) => {
            const {values, touched, errors, handleChange, handleBlur, handleSubmit, isValid, dirty} = props;
            return (
                <article className="blur br3 ba dark-gray b--white-10 mv4 w-100 w-50-m w-25-l  shadow-5 tc center">
                    <main className="pa4 white-80">
                        <div className="measure">
                            <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                                <legend className="f1 fw6 ph0 mh0">Sign In</legend>
                                <div className="mt3">
                                    <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                                    <input
                                        // placeholder="Enter your email"
                                        value={values.email}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={"pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 " + (errors.email && touched.email ? "error" : "")}
                                        type="email" name="email" id="email"/>
                                </div>
                                {errors.email && touched.email && (
                                    <div className="mt3 input-feedback f6 b">{errors.email}</div>
                                )}
                                <div className="mv3">
                                    <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                                    <input name="password"
                                           type="password"
                                        // placeholder="Enter your password"
                                           value={values.password}
                                           onChange={handleChange}
                                           onBlur={handleBlur}
                                           className={"b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 " + (errors.password && touched.password ? "error" : "")}
                                           id="password"/>
                                </div>
                            </fieldset>

                            {errors.password && touched.password && (
                                <div className="input-feedback f6 b">{errors.password}</div>
                            )}
                            <div className="">
                                <input disabled={!isValid || !dirty}
                                       className={"b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib "}
                                       type="submit" value="Sign in" onClick={handleSubmit}/>
                            </div>
                            <div className="lh-copy mt3">
                                <p onClick={() => onRouteChange(Constants.REGISTER_ROUTE)}
                                   className="f6 link dim white db pointer">Register</p>
                            </div>
                        </div>
                    </main>
                </article>
            );
        }}
    </Formik>
);
export default withAlert()(SignInForm);

//
// function SignIn({onRouteChange, loadUser, alert}) {
//     const [email, setemail] = useState('');
//     const [password, setPassword] = useState('');
//
//     function handleemailChange(event) {
//         setemail(event.target.value);
//     }
//
//     function handlePasswordChange(event) {
//         setPassword(event.target.value);
//     }
//
//
//     // const {onRouteChange} = props;
//     return (
//
//     )
// }
//
// export default withAlert()(SignIn);