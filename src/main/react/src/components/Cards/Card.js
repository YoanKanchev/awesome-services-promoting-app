import Tilt from "react-tilt/dist/tilt";

const Card = ({firstName, lastName, picture, email}) => {
    return (
            <Tilt className="bg-light-green link dib br3 pa3 ma3 grow bw2 shadow-5 Tilt " options={{max: 55}}
                  style={{height: 250, width: 250}}>
                <img className={"w-50 cover"} src={`${picture}`} alt="{}"/>
                <div className={"tc"}>
                    <h2>{`${firstName} ${lastName}`}</h2>
                    <p>{email}</p>
                </div>
            </Tilt>
    )
}

export default Card;