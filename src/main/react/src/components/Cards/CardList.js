import Card from "./Card";


const CardList = ({allUsers}) => {
    debugger
    return (
        <div className={'center'}>
            {allUsers.map((user) =>
                <Card
                    key={user.id}
                    id={user.id}
                    firstName={user.firstName}
                    lastName={user.lastName}
                    email={user.email}
                    picture={user.picture}
                />
            )}
        </div>
    )
}
export default CardList;