import * as Constants from '../../Constants'

function NavigationBar({onRoutChange, isSignedIn}) {
    if (isSignedIn) {
        return (
            <nav className={'flex justify-end'}>
                <p className={'f3 link dim white underline pa3 pointer'}
                   onClick={() => onRoutChange(Constants.SIGN_OUT_ROUTE)}>Sign
                    out</p>
            </nav>
        )
    } else {
        return (
            <nav className={'flex justify-end'}>
                <p className={'f3 link dim white underline pa3 pointer'}
                   onClick={() => onRoutChange(Constants.SIGN_IN_ROUTE)}>Sign
                    in</p>
                <p className={'f3 link dim white underline pa3 pointer'}
                   onClick={() => onRoutChange(Constants.REGISTER_ROUTE)}>Register</p>
            </nav>
        )
    }
}

export default NavigationBar;