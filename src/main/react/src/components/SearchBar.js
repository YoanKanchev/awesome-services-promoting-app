import React from "react";

const SearchBar = ({searchChange}) => {
    return (
        <div>
            <input className={"pa3 ba b--green bg-light-blue"}
                   type="text"
                   placeholder={"Search..."}
                   aria-describedby="name-desc"
                   onChange={searchChange}
        />
</div>
)
}
export default SearchBar;