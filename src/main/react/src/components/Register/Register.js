import * as Constants from "../../Constants"
import {Formik} from "formik";
import {types, withAlert} from "react-alert";
import * as Yup from "yup";
import React from "react";


const Register = ({onRouteChange, loadUser, alert}) => (
    <Formik
        initialValues={{email: "", firstName: "", lastName: "", password: "", confirmPassword: ""}}
        onSubmit={(values, {setSubmitting}) => {
            debugger
            fetch("/api/signup",
                {
                    method: "post",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                            email: values.email,
                            firstName: values.firstName,
                            lastName: values.lastName,
                            password: values.password,
                            confirmPassword: values.confirmPassword
                        }
                    )
                })
                .then(response => {
                    debugger
                   return  response.json()
                })
                .then(user => {
                    debugger
                    if (!user.error) {
                        alert.show("An confirmation email has been send!", {type: types.SUCCESS});
                        loadUser(user);
                        onRouteChange(Constants.HOME_ROUTE);
                        setSubmitting(true);
                    } else {
                        debugger
                        alert.show(user.message, {type: types.ERROR});
                    }
                })
                .catch(err => {
                    debugger
                        alert.show(err)
                    }
                )
        }}
        //********Using Yum for validation********/
        validationSchema={Yup.object().shape({
            email: Yup.string()
                .email("Must be a valid email")
                .required("Required!"),
            firstName: Yup.string()
                .required("Required!"),
            lastName: Yup.string()
                .required("Required!"),
            password: Yup.string()
                .required("Required!")
                .min(8, "Password is too short - should be 8 chars minimum.")
                .matches(/(?=.*[0-9])/, "Password must contain a number."),
            confirmPassword: Yup.string()
                .required("Required!")
                .oneOf([Yup.ref('password')], "Passwords must match")
        })}
    >
        {(props) => {
            const {values, touched, errors, handleChange, handleBlur, handleSubmit, isValid, dirty} = props;
            return (
                <article className="blur br3 ba dark-gray b--white-10 mv4 w-100 w-50-m w-25-l  shadow-5 tc center">
                    <main className="pa4 white-80">
                        <div className="measure">
                            <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                                <legend className="f1 fw6 ph0 mh0">Register</legend>
                                <div className="mt3">
                                    <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                                    <input
                                        value={values.email}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={"pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 "
                                        + (errors.email && touched.email ? "error" : "")}
                                        type="email" name="email" id="email"/>
                                </div>
                                {errors.email && touched.email && (
                                    <div className="mt1 input-feedback f6 b">{errors.email}</div>
                                )}
                                <div className="mt3">
                                    <label className="db fw6 lh-copy f6" htmlFor="first-name">First name</label>
                                    <input
                                        value={values.firstName}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={"pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 "
                                        + (errors.firstName && touched.firstName ? "error" : "")}
                                        type="text" name="firstName" id="firstName"/>
                                </div>
                                {errors.firstName && touched.firstName && (
                                    <div className="mt1 input-feedback f6 b">{errors.firstName}</div>
                                )}
                                <div className="mt3">
                                    <label className="db fw6 lh-copy f6" htmlFor="last-name">Last name</label>
                                    <input
                                        value={values.lastName}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={"pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 "
                                        + (errors.lastName && touched.lastName ? "error" : "")}
                                        type="text" name="lastName" id="lastName"/>
                                </div>
                                {errors.lastName && touched.lastName && (
                                    <div className="mt1 input-feedback f6 b">{errors.lastName}</div>
                                )}
                                <div className="mv3">
                                    <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                                    <input name="password"
                                           type="password"
                                           value={values.password}
                                           onChange={handleChange}
                                           onBlur={handleBlur}
                                           className={"b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 "
                                           + (errors.password && touched.password ? "error" : "")}
                                           id="password"/>
                                </div>
                                {errors.password && touched.password && (
                                    <div className="mt1 input-feedback f6 b">{errors.password}</div>
                                )}
                                <div className="mv3">
                                    <label className="db fw6 lh-copy f6" htmlFor="password-confirmation">Confirm
                                        password</label>
                                    <input name="confirmPassword"
                                           type="password"
                                           value={values.confirmPassword}
                                           onChange={handleChange}
                                           onBlur={handleBlur}
                                           className={"b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100 "
                                           + (errors.confirmPassword && touched.confirmPassword ? "error" : "")}
                                           id="confirmPassword"/>
                                </div>
                                {errors.confirmPassword && touched.confirmPassword && (
                                    <div className="mt1 input-feedback f6 b">{errors.confirmPassword}</div>
                                )}
                            </fieldset>
                            <div className="">
                                <input disabled={!isValid || !dirty}
                                       className={"b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib "}
                                       type="submit" value="Sign in" onClick={handleSubmit}/>
                            </div>
                        </div>
                    </main>
                </article>
            );
        }}
    </Formik>
);
export default withAlert()(Register);