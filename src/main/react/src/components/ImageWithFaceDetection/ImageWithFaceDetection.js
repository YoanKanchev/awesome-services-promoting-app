import "./ImageWithFaceDetection.css"

function ImageWithFaceDetection({imageInput, boxes}) {
    debugger
    let keyGen = 1
    return (
        <div className={'center ma'}>
            <div className={'absolute mt2'}>
                <img id="image" src={imageInput} alt={'pic'} width={'500px'} height={'auto'}/>
                <div>
                    {boxes.map((box) => <div key={++keyGen} className={'bounding-box'}
                                             style={{
                                                 top: box.topRow,
                                                 right: box.rightCol,
                                                 bottom: box.bottomRow,
                                                 left: box.leftCol
                                             }}/>)}
                </div>
            </div>
        </div>
    )
}


export default ImageWithFaceDetection;