export const DEFAULT_PICTURE_URL = "https://i.insider.com/5d321d4ea209d3146d650b4a?width=1100&format=jpeg&auto=webp";
export const HOME_ROUTE = 'home';
export const SIGN_IN_ROUTE = 'signIn';
export const SIGN_OUT_ROUTE = 'signOut';
export const REGISTER_ROUTE = 'register';