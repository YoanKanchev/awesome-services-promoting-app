import CardList from "./components/Cards/CardList";
import SearchBar from "./components/SearchBar";
import './App.css';
import React, {Component} from "react";
import Particles from "react-particles-js";
import NavigationBar from "./components/Navigation/NavigationBar";
import * as Const from './Constants';
import Register from "./components/Register/Register";
import SignIn from "./components/SignIn/SignIn";

const particleOptions = {
    particles: {
        number: {
            value: 70,
            density: {
                enable: true,
                value_area: 1000
            }
        }
    }
};



const initialState = {
    allUsers: [],
    searchField: '',
    route: Const.SIGN_IN_ROUTE,
    isSignedIn: false,
    loggedUser: {
        email: '',
        roles: [],
        jwt: ''
    },
    authorisationToken: ''
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    loadUser = (data) => {
        this.setState({
            user: {
                username: data.username,
                email: data.email,
                roles: data.roles,
                token: data.token
            }
        })
    }

    onSearchChange = (event) => {
        this.setState({searchField: event.target.value})
    }

    onRouteChange = (route) => {
        if (route === Const.SIGN_OUT_ROUTE) {
            this.setState(initialState)
        } else if (route === Const.HOME_ROUTE) {
            this.setState({isSignedIn: true})
        }
        this.setState({route: route})
    }

    componentDidMount() {
    }

    getUsers() {
        fetch("/api/users")
            .then(response => response.json())
            .then(users => this.setState({allUsers: users}))
    }

    filterUsers = () => {
        if (this.state.allUsers.length <= 0) {
            this.getUsers();
        }
        let searchString = this.state.searchField.toLowerCase();
        return this.state.allUsers.filter(
            (user) => {
                let fullName = `${user.firstName} ${user.lastName}`
                return fullName.toLowerCase().includes(searchString) || user.email.toLowerCase().includes(searchString)
            })
    }

    render() {
        const {isSignedIn, route} = this.state

        return (
            <div className={'App'}>
                <Particles params={particleOptions} className={'particles'}/>
                <NavigationBar onRoutChange={this.onRouteChange} isSignedIn={isSignedIn}/>
                {route === Const.HOME_ROUTE ?
                    <div className={"tc"}>
                        <h1 className={"f1"}>ROBOFRIENDS</h1>
                        <SearchBar searchChange={this.onSearchChange}/>
                        <CardList allUsers={this.filterUsers()}/>
                    </div>
                    :
                    (route === Const.REGISTER_ROUTE ?
                        <Register onRouteChange={this.onRouteChange}/> :
                        <SignIn onRouteChange={this.onRouteChange} loadUser={this.loadUser} signIn={this.signIn}/>)
                }
            </div>
        )
    }
}

export default App;