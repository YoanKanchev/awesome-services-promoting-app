package com.example.aservpro.enums;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum Role implements Comparable<Role> {
    ROLE_REGULAR(3),
    ROLE_AGENT(2),
    ROLE_ADMIN(1);

    private final int value;

    Role(int value) {
        this.value = value;
    }

    public static Set<String> getAll() {
        return Arrays.stream(values()).map(Enum::toString).collect(Collectors.toSet());
    }

    public int getValue() {
        return value;
    }
}
