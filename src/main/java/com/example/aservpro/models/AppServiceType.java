package com.example.aservpro.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "service_types")
public class AppServiceType implements Serializable, Comparable<AppServiceType> {

    @Id
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "service_id")
    private AppService service;

    @Id
    @Column(name = "type")
    private String type;

    @Override
    public int compareTo(AppServiceType o) {
        return this.type.compareTo(o.getType());
    }
}
