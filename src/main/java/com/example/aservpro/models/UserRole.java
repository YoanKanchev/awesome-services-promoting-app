package com.example.aservpro.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "user_roles")
public class UserRole implements GrantedAuthority {

    @Id
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @EqualsAndHashCode.Include
    @Column(name = "authority")
    private String authority;

    public UserRole(String authority) {
        this.authority = authority;
    }

    public UserRole(User user, String authority) {
        this.authority = authority;
        this.user = user;
    }
}
