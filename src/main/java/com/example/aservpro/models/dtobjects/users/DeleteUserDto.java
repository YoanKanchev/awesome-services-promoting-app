package com.example.aservpro.models.dtobjects.users;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class DeleteUserDto {

		@NotNull
		private Integer id;

		private String password;

		public DeleteUserDto(@NotNull Integer id) {
				this.id = id;
		}
}
