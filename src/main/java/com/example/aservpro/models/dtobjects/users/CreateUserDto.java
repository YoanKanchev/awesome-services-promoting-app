package com.example.aservpro.models.dtobjects.users;

import com.example.aservpro.helpers.anotations.PasswordValueMatch;
import com.example.aservpro.helpers.anotations.ValidPassword;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@PasswordValueMatch.List({
        @PasswordValueMatch(
                field = "password",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
@Data
public class CreateUserDto {

    private int Id;

    @Size(min = 4, max = 30, message = "Username must be between 4 and 30 characters!")
    private String username;

    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 characters!")
    private String firstName;

    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 characters!")
    private String lastName;

    @ValidPassword
    private String password;

    private String confirmPassword;

    @Email
    private String email;

    private String role = "ROLE_USER";

    private String picture;

    private String phone;

    private boolean isEnabled;
}
