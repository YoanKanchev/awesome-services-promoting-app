package com.example.aservpro.models.dtobjects;

public interface FilterParameters {
    Boolean isSorted();

    Boolean getIsOrderAsc();

    String getSortBy();
}
