package com.example.aservpro.models.dtobjects;

import lombok.Data;

@Data
public class EmailDtoText {

    private final String to;
    private final String subject;
    private final String message;

    public EmailDtoText(String to, String subject, String message) {
        this.to = to;
        this.subject = subject;
        this.message = message;
    }

}
