package com.example.aservpro.models.dtobjects.users;

import com.example.aservpro.models.dtobjects.FilterDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserFilterDto implements FilterDto {

		private String  usernameOrMail;
		private String  role;
		private String  isActive;
		private String  sortBy;
		private boolean orderAscending;

		@Override
		public boolean isSorted() {
				return sortBy != null && !sortBy.equals("-1");
		}
}
