package com.example.aservpro.models.dtobjects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppServiceFilterParameterDto implements FilterParameters {

    private final String type;
    private final String creatorName;
    private final Integer rating;
    private final String sortBy;
    private final Boolean isOrderAsc;

    public AppServiceFilterParameterDto(String type, String creatorName, Integer rating, String sortBy, Boolean isOrderAsc) {
        this.type = type;
        this.creatorName = creatorName;
        this.rating = rating;
        this.sortBy = sortBy;
        this.isOrderAsc = isOrderAsc;
    }

    @Override
    public Boolean isSorted() {
        return sortBy != null;
    }
}
