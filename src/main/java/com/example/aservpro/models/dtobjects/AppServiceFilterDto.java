package com.example.aservpro.models.dtobjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppServiceFilterDto {
    private String type;
    private String creatorName;
    private Integer rating;
    private String sortBy;
    private boolean isOrderAsc;
}
