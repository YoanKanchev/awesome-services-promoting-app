package com.example.aservpro.models.dtobjects;

public interface FilterDto {
		boolean isSorted();

		boolean isOrderAscending();

		String getSortBy();
}
