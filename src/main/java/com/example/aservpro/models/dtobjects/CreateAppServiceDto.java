package com.example.aservpro.models.dtobjects;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CreateAppServiceDto {

    @NotEmpty
    @Size(min = 4, max = 30, message = "Name of the service must be between 4 and 30 characters!")
    private String name;

    private String description;
    @Positive
    private Float price;
    @NotNull
    private String type;

    public CreateAppServiceDto() {
    }
}
