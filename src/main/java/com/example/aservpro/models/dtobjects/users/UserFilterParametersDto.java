package com.example.aservpro.models.dtobjects.users;

import com.example.aservpro.models.dtobjects.FilterParameters;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFilterParametersDto implements FilterParameters {
    private String usernameOrMail;
    private String role;
    private String sortBy;
    private Boolean isOrderAsc;

    @Override
    public Boolean isSorted() {
        return sortBy != null;
    }
}
