package com.example.aservpro.models.dtobjects;

import lombok.Data;

import java.util.Map;

@Data
public class EmailDtoHtml {

    private String to;
    private String subject;
    private String template;
    private Map<String, Object> templateModel;

    public EmailDtoHtml(String to, String subject, String template, Map<String, Object> templateParameters) {
        this.to = to;
        this.subject = subject;
        this.template = template;
        this.templateModel = templateParameters;
    }
}
