package com.example.aservpro.models.dtobjects.users;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UpdateUserDto {

    private Integer id;

    @Email
    private String email;

    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 characters!")
    private String firstName;

    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 characters!")
    private String lastName;

    @NotEmpty
    private String password;

    private String role = "ROLE_USER";

    private String picture;

    private String phone;

    private boolean isActive;
}
