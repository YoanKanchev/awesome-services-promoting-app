package com.example.aservpro.models.dtobjects.users;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class UpdateUserFromAdminDto {

    private Integer id;

    @Size(min = 4, max = 30, message = "Username must be between 4 and 30 characters!")
    private String username;

    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 characters!")
    private String firstName;

    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 characters!")
    private String lastName;

    private String addRole;

    private String removeRole;

    private boolean isPictureReset;

    private boolean isEnabled;
}
