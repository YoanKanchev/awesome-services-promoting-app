package com.example.aservpro.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter(AccessLevel.PRIVATE)
@Entity
@Table(name = "confirmation_tokens")
public class ConfirmationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "token")
    private String tokenValue;

    @Column(name = "created_date")
    private Date createDate;

    @Column(name = "expiration_date")
    private Date expirationDate;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    public ConfirmationToken(String tokenValue, User user, int expirationTimeInMinutes) {
        this.user = user;
        createDate = new Date();
        setExpirationDate(createDate, expirationTimeInMinutes);
        this.tokenValue = tokenValue;
    }

    public ConfirmationToken() {
    }

    private void setExpirationDate(Date createDate, int expirationTimeInMinutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(createDate);
        calendar.add(Calendar.MINUTE, expirationTimeInMinutes);
        this.expirationDate = calendar.getTime();
    }

    public boolean isExpired() {
        long expirationDateMilliseconds = expirationDate.getTime();
        long currentDateMilliseconds = new Date().getTime();
        return currentDateMilliseconds > expirationDateMilliseconds;
    }
}
