package com.example.aservpro.models.interfaces;

import com.example.aservpro.models.User;

public interface EntityWithCreator {

    User getCreator();
}
