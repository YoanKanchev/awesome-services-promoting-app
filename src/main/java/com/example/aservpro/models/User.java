package com.example.aservpro.models;

import com.example.aservpro.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User implements UserDetails {

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "favourite_services",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private final Set<AppService> favouriteServices = new HashSet<>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "email")
    private String email;
    @JsonIgnore
    @Column(name = "password")
    private String password;
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<UserRole> authorities;
    @Column(name = "picture")
    private String picture;
    @Column(name = "enabled")
    private boolean enabled;
    @JsonIgnore
    @Transient
    private boolean accountNonExpired;
    @JsonIgnore
    @Transient
    private boolean accountNonLocked;
    @JsonIgnore
    @Transient
    private boolean credentialsNonExpired;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;
    @Column(name = "registered_date")
    private String registrationDate;
    @Column(name = "deactivation_date")
    private String deactivationDate;
    @Column(name = "last_edited_date")
    private String updateDate;


    public User() {
        setAccountNonExpired(true);
        setAccountNonLocked(true);
        setCredentialsNonExpired(true);
        setEnabled(false);
    }

    public List<AppService> getFavouriteServices() {
        return new ArrayList<>(favouriteServices);
    }

    public void addServiceToFavourite(AppService service) {
        favouriteServices.add(service);
    }

    public void removeServiceFromFavourite(AppService service) {
        favouriteServices.remove(service);
    }

    public boolean hasRole(Role role) {
        return authorities.contains(new UserRole(role.toString()));
    }

    public void deactivate() {
        setEnabled(false);
        setUpdateDate(LocalDate.now().toString());
        setDeactivationDate(LocalDate.now().toString());
    }

    public void activate() {
        setEnabled(true);
        setDeactivationDate(null);
        setUpdateDate(LocalDate.now().toString());
    }

    public void addAuthorities(UserRole... authority) {
        authorities.addAll(Arrays.asList(authority));
    }

    public void removeAuthorities(UserRole... authority) {
        authorities.removeAll(Arrays.asList(authority));
    }

    public String getHighestAuthority() {
        LinkedList<Role> authorities = this.authorities.stream()
                .map(role -> Role.valueOf(role.getAuthority())).sorted(Comparator.comparingInt(Role::getValue))
                .collect(Collectors.toCollection(LinkedList::new));

        return authorities.isEmpty() ? null : authorities.getFirst().toString();
    }

    public boolean hasAuthority(Role authority) {
        return authorities.stream()
                .anyMatch(userRole ->
                        userRole.getAuthority().equals(authority.toString()));
    }

    @Override
    public String getUsername() {
        return this.email;
    }
}
