package com.example.aservpro.models;

import com.example.aservpro.models.interfaces.EntityWithCreator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "services")
public class AppService  implements EntityWithCreator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @EqualsAndHashCode.Include
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<AppServiceType> types;

    @Column(name = "price")
    private Double price;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "user_id")
    private User creator;

    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<AppServiceRating> ratings;

    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<AppServiceComment> comments;

    @Column(name = "published_date")
    private Timestamp publishedDate;

    @Column(name = "views")
    private Integer views;

    @Formula("(SELECT AVG(sr.rating) FROM services_ratings sr WHERE sr.service_id = id)")
    private Double avgRating;

    public void incrementViews() {
        this.views++;
    }

    public String getCreatorUsername() {
        return this.creator.getUsername();
    }

}
