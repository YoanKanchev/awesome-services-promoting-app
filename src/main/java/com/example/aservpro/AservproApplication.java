package com.example.aservpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AservproApplication {

    public static void main(String[] args) {
        SpringApplication.run(AservproApplication.class, args);
    }
}
