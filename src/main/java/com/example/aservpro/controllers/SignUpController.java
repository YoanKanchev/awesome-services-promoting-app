package com.example.aservpro.controllers;

import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.users.CreateUserDto;
import com.example.aservpro.services.ConfirmationTokenService;
import com.example.aservpro.services.EmailService;
import com.example.aservpro.services.UserServiceActions;
import com.example.aservpro.services.UserServiceFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/signup")
public class SignUpController {

    private final UserServiceActions userServiceActions;
    private final UserServiceFinder userServiceFinder;
    private final EmailService emailService;
    private final ConfirmationTokenService tokenService;

    @Autowired
    public SignUpController(UserServiceActions userServiceActions, UserServiceFinder userServiceFinder,
                            EmailService emailService, ConfirmationTokenService tokenService) {
        this.userServiceActions = userServiceActions;
        this.userServiceFinder = userServiceFinder;
        this.emailService = emailService;
        this.tokenService = tokenService;
    }

    @PostMapping
    public User create(@Validated @ModelAttribute CreateUserDto userDto, HttpServletRequest request) throws MessagingException {
        User newUser = userServiceActions.create(userDto);
        sendRegistrationConfirmationEmail(newUser.getEmail(), request);
        return newUser;
    }

    @PostMapping("/send-confirmation")
    public void sendRegistrationConfirmationEmail(@RequestParam String email, HttpServletRequest servletRequest) throws MessagingException {
        String urlForMail = buildUrlForAccountActivation(servletRequest);
        User user = userServiceFinder.getByEmail(email);
        ConfirmationToken token = tokenService.newToken(user);
        emailService.sendRegistrationConfirmationEmail(token, urlForMail);
    }

    @GetMapping("/verify")
    public void confirmUserAccount(@RequestParam String tokenValue) {
            ConfirmationToken token = tokenService.getByTokenValue(tokenValue);
            User user = token.getUser();
            userServiceActions.activateWithToken(user, token);
    }

    private String buildUrlForAccountActivation(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/api/signup/verify?tokenValue=";
    }
}
