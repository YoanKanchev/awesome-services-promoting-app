package com.example.aservpro.controllers;

import com.example.aservpro.config.JwtTokenUtil;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.users.LoginDto;
import com.example.aservpro.services.UserServiceFinder;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil tokenProvider;
    private final UserServiceFinder userServiceFinder;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, JwtTokenUtil tokenProvider, UserServiceFinder userServiceFinder) {
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.userServiceFinder = userServiceFinder;
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginDto loginData) {
        JSONObject jsonObject = new JSONObject();
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginData.getEmail(), loginData.getPassword()));
            if (authentication.isAuthenticated()) {
                User user = userServiceFinder.getByEmail(loginData.getEmail());
                jsonObject.put("email", user.getEmail());
               // List<String> authorities = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
                jsonObject.put("role", user.getHighestAuthority());
                jsonObject.put("token", tokenProvider.createToken(user.getEmail(), user.getHighestAuthority()));

                return ResponseEntity.ok(jsonObject.toString());
            }
        } catch (JSONException  e) {
            try {
                jsonObject.put("exception", e.getMessage());
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNAUTHORIZED);
        }
        return null;
    }
}
