package com.example.aservpro.controllers;

import com.example.aservpro.models.AppService;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.AppServiceFilterDto;
import com.example.aservpro.models.dtobjects.AppServiceFilterParameterDto;
import com.example.aservpro.services.AppServiceService;
import com.example.aservpro.services.AppServiceServiceImpl;
import com.example.aservpro.services.UserServiceActions;
import com.example.aservpro.services.UserServiceFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/services")
public class ServiceController {

    private final AppServiceService appServiceService;
    private final UserServiceFinder userServiceFinder;
    private final UserServiceActions userServiceActions;

    @Autowired
    public ServiceController(AppServiceServiceImpl appServiceService, UserServiceFinder userServiceFinder,
                             UserServiceActions userServiceActions) {
        this.appServiceService = appServiceService;
        this.userServiceFinder = userServiceFinder;
        this.userServiceActions = userServiceActions;
    }

    @GetMapping()
    public List<AppService> getAll() {
        return appServiceService.getAll();
    }

    @GetMapping("/{id}")
    public AppService getById(@PathVariable int id) {
        return appServiceService.getById(id);
    }

    @GetMapping("/")
    public List<AppService> filter(AppServiceFilterDto appServiceFilterDto) {
        return appServiceService.filter(new AppServiceFilterParameterDto(appServiceFilterDto.getType(),
                appServiceFilterDto.getCreatorName(), appServiceFilterDto.getRating(), appServiceFilterDto.getSortBy(),
                appServiceFilterDto.isOrderAsc()));
    }

    @PostMapping("/{id}/addFav")
    public void addToFavourite(@PathVariable int id, Principal principal) {
        User user = userServiceFinder.getByEmail(principal.getName());
        AppService appService = appServiceService.getById(id);
        userServiceActions.addServiceToFavourite(user, appService);
    }

    @PostMapping("/{id}/removeFav")
    public void removeFromFavourite(@PathVariable int id, Principal principal) {
        User user = userServiceFinder.getByEmail(principal.getName());
        AppService appService = appServiceService.getById(id);
        userServiceActions.removeServiceFromFav(user, appService);
    }
}
