package com.example.aservpro.controllers;

import com.example.aservpro.exceptions.AuthorisationException;
import com.example.aservpro.exceptions.DuplicateEntityException;
import com.example.aservpro.exceptions.EntityNotFoundException;
import com.example.aservpro.models.AppService;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.users.CreateUserDto;
import com.example.aservpro.models.dtobjects.users.DeleteUserDto;
import com.example.aservpro.models.dtobjects.users.UpdateUserDto;
import com.example.aservpro.models.dtobjects.users.UserFilterParametersDto;
import com.example.aservpro.services.UserServiceActions;
import com.example.aservpro.services.UserServiceFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/users")
public class UserController {

    private final UserServiceFinder userServiceFinder;
    private final UserServiceActions userServiceActions;

    @Autowired
    public UserController(UserServiceFinder userServiceFinder, UserServiceActions userServiceActions) {
        this.userServiceFinder = userServiceFinder;
        this.userServiceActions = userServiceActions;
    }

    @GetMapping()
    public List<User> getAll() {
        return userServiceFinder.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
            return userServiceFinder.getById(id);
    }

    @GetMapping("/")
    public List<User> filter(UserFilterParametersDto userFilterParametersDto) {
        return userServiceFinder.filter(userFilterParametersDto);
    }

    @PostMapping("/")
    public User createUser(@Valid @ModelAttribute CreateUserDto userDto) {
            return userServiceActions.create(userDto);
    }

    @PutMapping("/")
    public User update(@Valid @RequestBody UpdateUserDto updateData, Principal principal) {
            User loggedUser = userServiceFinder.getByEmail(principal.getName());
            return userServiceActions.update(updateData, loggedUser);
    }

    @DeleteMapping("/")
    public void delete(DeleteUserDto userData, Principal principal) {
            User loggedUser = userServiceFinder.getByEmail(principal.getName());
            userServiceActions.deactivate(userData, loggedUser);
    }

    @GetMapping("/favServ")
    public List<AppService> getFavouriteServices(Principal principal) {
            User loggedUser = userServiceFinder.getByEmail(principal.getName());
            return userServiceFinder.getFavouriteServices(loggedUser);
    }
}
