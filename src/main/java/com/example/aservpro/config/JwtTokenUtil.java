package com.example.aservpro.config;

import com.example.aservpro.services.security.MyUserDetailsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.function.Function;

import static com.example.aservpro.helpers.GlobalConstants.JWT_TOKEN_EXPIRATION_MINUTES;

@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = 2569800841756370596L;

//    private final AuthenticationManager authenticationManager;
    private final MyUserDetailsService userDetailsService;
    private final String secretKey;

    @Autowired
    public JwtTokenUtil(MyUserDetailsService userDetailsService, Environment env) {

        this.userDetailsService = userDetailsService;
        this.secretKey = encodeKey(env.getProperty("jwt.secret-key"));
    }

    private String encodeKey(String property) {
        return Strings.isNotEmpty(property) ? Base64.getEncoder().encodeToString(property.getBytes()) : null;
    }


    public String createToken(String username, String authority) {
        Date now = new Date();
        Date expirationDate = DateUtils.addMinutes(now, JWT_TOKEN_EXPIRATION_MINUTES);
        return Jwts.builder()
                .setSubject(username)
                .claim("auth", authority)
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsernameFromToken(token));
        return new UsernamePasswordAuthenticationToken(
                userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
    }

    public Claims getClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }

    // retrieve username from jwt token
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    // retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public Boolean isTokenExpired(String token) {
        Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    // validate token
    public void validateTokenNotExpired(String token) {
        if (isTokenExpired(token)){
            throw new JwtException("Token is expired");
        }
    }
}
