package com.example.aservpro.config;

import org.apache.logging.log4j.util.Strings;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.example.aservpro.helpers.GlobalConstants.JWT_TOKEN_PREFIX;
import static com.example.aservpro.helpers.GlobalConstants.REQUEST_HEADER_AUTHORISATION;

@Component
public class JwtTokenFilter extends OncePerRequestFilter {
    private final JwtTokenUtil tokenProvider;

    @Autowired
    public JwtTokenFilter(JwtTokenUtil tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String requestHeader = request.getHeader(REQUEST_HEADER_AUTHORISATION);

        if (Strings.isEmpty(requestHeader)) {
            filterChain.doFilter(request, response);
            return;
        }

        try {
            //removing bearer prefix
            String token = requestHeader.replace(JWT_TOKEN_PREFIX, "");

            tokenProvider.validateTokenNotExpired(token);
            Authentication authentication = tokenProvider.getAuthentication(token);
            if (authentication.isAuthenticated()) {
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }

        } catch (RuntimeException e) {
            try {
                SecurityContextHolder.clearContext();
                response.setContentType("application/json");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().println(
                        new JSONObject().put("exception", e.getMessage()));
            } catch (IOException | JSONException e1) {
                e1.printStackTrace();
            }
        }
    }
}
