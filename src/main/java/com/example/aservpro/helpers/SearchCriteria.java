package com.example.aservpro.helpers;

public class SearchCriteria {
	private Object value;
	private Object searchBy;

	public SearchCriteria(Object value, Object operation) {
		this.value = value;
		this.searchBy = operation;
	}

	public String getValue() {
		return value.toString();
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getSearchBy() {
		return searchBy.toString();
	}

	public void setSearchBy(Object operation) {
		this.searchBy = operation;
	}
}
