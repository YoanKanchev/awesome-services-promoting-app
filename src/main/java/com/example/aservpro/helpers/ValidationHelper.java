package com.example.aservpro.helpers;

import com.example.aservpro.exceptions.AuthorisationException;
import com.example.aservpro.exceptions.DuplicateEntityException;
import com.example.aservpro.models.AppService;
import com.example.aservpro.models.User;
import com.example.aservpro.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.example.aservpro.helpers.GlobalConstants.ALREADY_TAKEN_MESSAGE;


@Component
public class ValidationHelper {
    private final UserRepository userRepository;

    @Autowired
    public ValidationHelper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

//    public void validateCreatorOrAdmin(AppService service, User loggedUser) {
//        boolean isLoggedUserCreator = loggedUser.getUsername().equals(service.getCreatorUsername());
//        boolean loggedUserIsAdmin = loggedUser.hasAuthority(ROLE_ADMIN);
//        if (!isLoggedUserCreator && !loggedUserIsAdmin)
//            throw new AuthorisationException("You are not authorised for this operation");
//    }
//
//    public void validateLoggedUserOrAdmin(User user, User loggedUser) {
//        boolean isUpdatedUserTheLogged = user.getUsername().equals(loggedUser.getUsername());
//        boolean loggedUserIsAdmin = loggedUser.hasAuthority(ROLE_ADMIN);
//        if (!isUpdatedUserTheLogged && !loggedUserIsAdmin)
//            throw new AuthorisationException("You are not authorised for this operation");
//    }
//
//
//    public void userMailDuplicationValidator(String email) {
//        Optional<User> userCheck = userRepository.findByEmail(email);
//        duplicationChecker(email, userCheck, String.format((ALREADY_TAKEN_MESSAGE), "Email:" + email));
//    }
//
//    public void usernameDuplicationValidator(String username, String usernameToCheck) {
//        Optional<User> userCheck = userRepository.findByUsername(username);
//        duplicationChecker(usernameToCheck, userCheck, String.format((ALREADY_TAKEN_MESSAGE), "Username:" + username));
//    }
//
//    public void usernameDuplicationValidator(String username) {
//        Optional<User> userCheck = userRepository.findByUsername(username);
//        duplicationChecker(userCheck, String.format((ALREADY_TAKEN_MESSAGE), "Username:" + username));
//    }
//
//    private void duplicationChecker(String email, Optional<User> userCheck, String error) {
//        if (userCheck.isPresent() && !email.equals(userCheck.get().getEmail()))
//            throw new DuplicateEntityException(error);
//    }
//
//    private void duplicationChecker(Optional<User> userCheck, String error) {
//        if (userCheck.isPresent())
//            throw new DuplicateEntityException(error);
//    }
}
