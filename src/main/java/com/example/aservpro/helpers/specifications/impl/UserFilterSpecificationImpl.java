package com.example.aservpro.helpers.specifications.impl;

import com.example.aservpro.helpers.SearchCriteria;
import com.example.aservpro.helpers.enums.UserSearchOperation;
import com.example.aservpro.helpers.specifications.FilterSpecification;
import com.example.aservpro.models.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class UserFilterSpecificationImpl implements FilterSpecification<User> {

    private static final long serialVersionUID = 1900581010229669687L;
    private final List<SearchCriteria> list;

    public UserFilterSpecificationImpl() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        // create a new predicate list
        List<Predicate> predicates = new ArrayList<>();
        // add add criteria to predicates
        for (SearchCriteria criteria : list) {
            UserSearchOperation searchBy = UserSearchOperation.valueOf(criteria.getSearchBy());
            String value = criteria.getValue();
            switch (searchBy) {
                case USERNAMEOREMAIL: {
                    predicates.add(getPredicatesForUsernameOrMail(root, builder, value));
                }
                break;
                case ROLE:
                    predicates.add(getPredicatesForRole(root, builder, value));
                    break;
            }
        }
        return builder.and(predicates.toArray(new Predicate[0]));
    }

    private Predicate getPredicatesForUsernameOrMail(Root<User> root, CriteriaBuilder builder, String value) {
        Predicate username = builder.like(root
                .get("userAuthentication")
                .get("username"), "%" + value + "%");
        Predicate email = builder.like(root.get("email"), "%" + value + "%");
        return builder.or(username, email);
    }

    private Predicate getPredicatesForRole(Root<User> root, CriteriaBuilder builder, String value) {
        return builder.equal(root
                .join("userAuthentication")
                .join("authorities")
                .get("authority"), value);
    }
}
