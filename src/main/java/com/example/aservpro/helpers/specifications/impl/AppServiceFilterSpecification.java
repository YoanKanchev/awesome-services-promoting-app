package com.example.aservpro.helpers.specifications.impl;

import com.example.aservpro.helpers.SearchCriteria;
import com.example.aservpro.helpers.enums.AppServiceSearchOperation;
import com.example.aservpro.helpers.specifications.FilterSpecification;
import com.example.aservpro.models.AppService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class AppServiceFilterSpecification implements FilterSpecification<AppService> {

    private static final long serialVersionUID = 1900581010229669687L;
    private final List<SearchCriteria> list;

    public AppServiceFilterSpecification() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }

    @Override
    public Predicate toPredicate(Root<AppService> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        // create a new predicate list
        List<Predicate> predicates = new ArrayList<>();
        // add add criteria to predicates
        for (SearchCriteria criteria : list) {
            AppServiceSearchOperation searchBy = AppServiceSearchOperation.valueOf(criteria.getSearchBy());
            String value = criteria.getValue();

            switch (searchBy) {
                case TYPE: {
                    predicates.add(getPredicatesForType(root, builder, value));
                    break;
                }
                case CREATOR: {
                    predicates.add(getPredicatesForCreator(root, builder, value));
                    break;
                }
                case GREATER_OR_EQUAL_TO_RATING: {
                    predicates.add(getPredicatesForGreaterOrEqualRating(root, builder, value));
                    break;
                }
            }
        }
        return builder.and(predicates.toArray(new Predicate[0]));
    }

    private Predicate getPredicatesForGreaterOrEqualRating(Root<AppService> root, CriteriaBuilder builder, String value) {
        return builder.greaterThanOrEqualTo(root.get("avgRating"), Double.parseDouble(value));
    }

    private Predicate getPredicatesForCreator(Root<AppService> root, CriteriaBuilder builder, String value) {
        return builder.like(root.join("creator").join("userAuthentication").get("username"), value);
    }

    private Predicate getPredicatesForType(Root<AppService> root, CriteriaBuilder builder, String value) {
        return builder.like(root.join("types").get("type"), value);
    }
}
