package com.example.aservpro.helpers.specifications;

import com.example.aservpro.helpers.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public interface FilterSpecification<T> extends Specification<T> {

		void add(SearchCriteria criteria);

		@Override
		Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder);
}
