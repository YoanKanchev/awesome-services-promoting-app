package com.example.aservpro.helpers.enums;

public enum AppServiceSearchOperation {
    TYPE,
    CREATOR,
    GREATER_OR_EQUAL_TO_RATING
}
