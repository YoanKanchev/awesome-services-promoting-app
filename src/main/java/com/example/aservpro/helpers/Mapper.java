package com.example.aservpro.helpers;

import com.example.aservpro.models.AppService;
import com.example.aservpro.models.UserRole;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.CreateAppServiceDto;
import com.example.aservpro.models.dtobjects.users.CreateUserDto;
import com.example.aservpro.models.dtobjects.users.UpdateUserDto;
import com.example.aservpro.models.dtobjects.users.UpdateUserFromAdminDto;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Set;

@Component
public class Mapper {

    public User toUser(CreateUserDto userDto) {
        User user = new User();
        UserRole authority = new UserRole(user, userDto.getRole());

        user.setAuthorities(Set.of(authority));
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhone(userDto.getPhone());
        return user;
    }

    public User toUser(UpdateUserDto updateData, User userToUpdate) {
        userToUpdate.setEmail(updateData.getEmail());
        userToUpdate.setFirstName(updateData.getFirstName());
        userToUpdate.setLastName(updateData.getLastName());
        userToUpdate.setPicture(updateData.getPicture());
        userToUpdate.setPhone(updateData.getPhone());
        userToUpdate.setUpdateDate(LocalDate.now().toString());
        return userToUpdate;
    }

    @Transactional
    public User toUser(UpdateUserFromAdminDto updateData, User userToUpdate) {
        userToUpdate.setFirstName(updateData.getFirstName());
        userToUpdate.setLastName(updateData.getLastName());
        userToUpdate.setEmail(userToUpdate.getEmail());
        userToUpdate.setPicture(userToUpdate.getPicture());
        userToUpdate.setUpdateDate(LocalDate.now().toString());

        if (Strings.isNotEmpty(updateData.getAddRole())) {
            UserRole role = new UserRole(userToUpdate, updateData.getAddRole());
            userToUpdate.addAuthorities(role);
        }
        if (Strings.isNotEmpty(updateData.getRemoveRole())) {
            userToUpdate.removeAuthorities(new UserRole(updateData.getRemoveRole()));
        }
        return userToUpdate;
    }

    public AppService mapToAppService(CreateAppServiceDto serviceDto) {
        AppService appService = new AppService();
        appService.setName(serviceDto.getName());
        appService.setDescription(serviceDto.getDescription());
        appService.setPrice(appService.getPrice());
        return appService;
    }

}
