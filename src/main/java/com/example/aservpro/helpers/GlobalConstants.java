package com.example.aservpro.helpers;

public class GlobalConstants {
    public static final String USER_NOT_FOUND = "User with %s not found!";
    public static final String ALREADY_TAKEN_MESSAGE = "%s is already taken!";
    public static final String UNAUTHORISED_ERROR_MESSAGE = "You are not authorised for this operation";

    //CREATE USER DTO
    public static final String USERNAME_RESTRICTIONS = "Username must be between 4 and 30 characters!";
    public static final String FIRST_NAME_RESTRICTIONS = "First name must be between 2 and 20 characters!";
    public static final String LAST_NAME_RESTRICTIONS = "Last name must be between 2 and 20 characters!";
    //JWT TOKEN
    public static final int JWT_TOKEN_EXPIRATION_MINUTES = 1440;
    public static final String JWT_TOKEN_PREFIX = "Bearer ";
    //REQUEST_HEADER
    public static final String REQUEST_HEADER_AUTHORISATION = "Authorization";

    //LINK
    public static final String DEFAULT_PICTURE = "https://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg";

    //URL PATHS
    public static final String PATH_FOR_PASSWORD_RECOVERY_HANDLER = "/sign-up/recover-password/verify?token=";
    public static final String PATH_FOR_REGISTRATION_CONFIRMATION = "/sign-up/verify?token=";

    //HTML PAGES
    public static final String TEMPLATE_FOR_PASSWORD_RECOVERY = "password-reset-email-template.html";
    public static final String TEMPLATE_FOR_REGISTRATION_CONFIRMATION = "registration-confirmation-email-template.html";

    //EMAIL
    public static final String SUBJECT_FOR_REGISTRATION_CONFIRMATION = "ASERVPRO New Account Confirmation";
    public static final String SUBJECT_FOR_PASSWORD_RECOVERY = "ASERVPRO Password Recovery";
    public static final String CONFIRMATION_EMAIL_SEND = "An confirmation e-mail has been send to your e-mail address, please follow the link before continuing.";
    public static final String EMAIL_SEND = "An e-mail has been send to your e-mail address!";
    public static final String YOUR_LINK_HAS_EXPIRED = "Your link has expired...";

    //ENTITIES
    public static final String USER_ENTITY = "User";
    public static final String SERVICE_ENTITY = "Service";
    public static final String TOKEN_ENTITY = "Token";
    public static final String COUNTRY_ENTITY = "Country";
    public static final String RATING_ENTITY = "Rating";

    //MESSAGES
    public static final String NOT_FOUND = " not found!";
    public static final String SUCCESSFULLY_CREATED = " successfully created!";
    public static final String SUCCESSFULLY_UPDATED = " successfully updated!";
    public static final String SUCCESSFULLY_DELETED = " successfully deleted!";
    public static final String SUCCESSFULLY_ACTIVATE = " successfully activated!";
    public static final String SUCCESSFULLY_RATED = " successfully rated!";
    public static final String ALREADY_EXISTS = " already exists!";
    public static final String WITH_ID = " with id:%d ";
    public static final String WITH_NAME = " with name:%s ";
    public static final String WITH_EMAIL = " with email:%s ";
    public static final String YOU_ARE_UNAUTHORISED = "You are unauthorised!!";
    public static final String WRONG_CONFIRMATION_PASSWORD = "Wrong confirmation password!";

    //ACTIONS
    public static final String DELETE = "delete";
    public static final String UPDATE = "update";

    //SORT BY
    public static final String NAME = "name";
    public static final String AVG_RATING = "avgRating";
    public static final String BREWERY = "brewery";

    //USER ROLES
    public static final String INVALID_ROLE = "Invalid Role";

    //MVC MODEL ATTRIBUTE NAMES
    public static final String LOGGED_USER = "loggedUser";
    public static final String STYLES = "styles";
    public static final String COUNTRIES = "countries";
    public static final String USER = "user";
    public static final String RATING = "rating";

    //Entity fields (connected with database)
    public static final String ID = "id";
    public static final String COUNTRY = "country";
    public static final String ACTIVE = "active";
    public static final String USERNAME = "username";
    public static final String ENABLED = "enabled";
    public static final String EMAIL = "email";
    public static final String AUTHORITIES = "authorities";
    public static final String AUTHORITY = "authority";
    public static final int START_PAGE_PAGINATION = 1;
    public static final int NUMBER_OF_ITEMS_PER_PAGE_PAGINATION = 5;

    //REDIRECT ATTRIBUTES
    public static final String REDIRECT = "redirect:";
    public static final String REDIRECT_HOME = "redirect:/";
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";

    //ENCODING
    public static final String UTF_8 = "UTF-8";
}

