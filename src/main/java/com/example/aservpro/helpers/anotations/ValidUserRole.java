package com.example.aservpro.helpers.anotations;

import com.example.aservpro.helpers.anotations.anotationValidators.UserRoleValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {UserRoleValidator.class})
public @interface ValidUserRole {

    String message() default "Incorrect User Role!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
