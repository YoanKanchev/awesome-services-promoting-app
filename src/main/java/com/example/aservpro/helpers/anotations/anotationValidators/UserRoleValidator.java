package com.example.aservpro.helpers.anotations.anotationValidators;

import com.example.aservpro.enums.Role;
import com.example.aservpro.helpers.anotations.ValidUserRole;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class UserRoleValidator implements ConstraintValidator<ValidUserRole, String > {
    @Override
    public void initialize(ValidUserRole constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Arrays.stream(Role.values()).anyMatch(x->x.toString().equals(value));
    }
}
