package com.example.aservpro.exceptions;

public class AuthorisationException extends RuntimeException {
    public AuthorisationException(String message) {
        super(message);
    }
}
