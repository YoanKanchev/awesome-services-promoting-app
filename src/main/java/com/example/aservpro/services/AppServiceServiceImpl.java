package com.example.aservpro.services;

import com.example.aservpro.exceptions.EntityNotFoundException;
import com.example.aservpro.helpers.FilterBuilder;
import com.example.aservpro.helpers.Mapper;
import com.example.aservpro.helpers.specifications.FilterSpecification;
import com.example.aservpro.models.AppService;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.AppServiceFilterParameterDto;
import com.example.aservpro.models.dtobjects.CreateAppServiceDto;
import com.example.aservpro.repositories.AppServiceRepository;
import com.example.aservpro.services.validation.BaseValidationsService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.aservpro.helpers.GlobalConstants.*;

@Service
public class AppServiceServiceImpl implements AppServiceService {
    private final AppServiceRepository appServiceRepository;
    private final FilterBuilder filterBuilder;
    private final BaseValidationsService baseValidationsService;
    private final Mapper mapper;

    public AppServiceServiceImpl(AppServiceRepository appServiceRepository, FilterBuilder filterBuilder,
                                 BaseValidationsService baseValidationsService, Mapper mapper) {
        this.appServiceRepository = appServiceRepository;
        this.filterBuilder = filterBuilder;
        this.baseValidationsService = baseValidationsService;
        this.mapper = mapper;
    }

    @Override
    public List<AppService> getAll() {
        return appServiceRepository.findAll();
    }

    @Override
    public AppService getById(int id) {
        return appServiceRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(SERVICE_ENTITY + WITH_ID + NOT_FOUND, id)));
    }

    @Override
    public AppService create(CreateAppServiceDto serviceDto, User creator) {
        AppService serviceToCreate = mapper.mapToAppService(serviceDto);
        serviceToCreate.setCreator(creator);
        return appServiceRepository.save(serviceToCreate);
    }

    @Override
    public AppService update(AppService service, User loggedUser) {
        baseValidationsService.adminORCreatorCheck(service, loggedUser);
        return appServiceRepository.save(service);
    }

    @Override
    public void delete(AppService service, User loggedUser) {
        baseValidationsService.adminORCreatorCheck(service, loggedUser);
        appServiceRepository.delete(service);
    }

    @Override
    public List<AppService> filter(AppServiceFilterParameterDto appServiceFilterParameterDto) {
        FilterSpecification<AppService> filterSpecification = filterBuilder.createSpecification(appServiceFilterParameterDto);
        Sort sort = filterBuilder.getSortOrders(appServiceFilterParameterDto);
        return appServiceRepository.findAll(filterSpecification, sort);
    }

    @Override
    public AppService incrementViews(AppService appService) {
        appService.incrementViews();
        return appServiceRepository.save(appService);
    }
}
