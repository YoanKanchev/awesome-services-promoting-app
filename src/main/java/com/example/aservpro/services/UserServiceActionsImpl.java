package com.example.aservpro.services;

import com.example.aservpro.helpers.Mapper;
import com.example.aservpro.models.AppService;
import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.users.CreateUserDto;
import com.example.aservpro.models.dtobjects.users.DeleteUserDto;
import com.example.aservpro.models.dtobjects.users.UpdateUserDto;
import com.example.aservpro.repositories.UserRepository;
import com.example.aservpro.services.validation.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;

import static com.example.aservpro.enums.Role.ROLE_ADMIN;

@Service
public class UserServiceActionsImpl implements UserServiceActions {

    private final UserRepository userRepository;
    private final UserServiceFinder userServiceFinder;
    private final UserValidationService userValidationService;
    private final Mapper mapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceActionsImpl(UserRepository userRepository, UserServiceFinder userServiceFinder, UserValidationService userValidationService, Mapper mapper,
                                  PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userServiceFinder = userServiceFinder;
        this.userValidationService = userValidationService;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;

    }

    @Override
    @Transactional
    public User create(CreateUserDto userDto) {

        userValidationService.validateUniqueEmail(userDto.getEmail());
        String encodedPassword = passwordEncoder.encode(userDto.getPassword());
        User userToCreate = mapper.toUser(userDto);
        userToCreate.setPassword(encodedPassword);
        userToCreate.setRegistrationDate(LocalDate.now().toString());

        return userRepository.save(userToCreate);
    }

    @Override
    public User update(UpdateUserDto updateData, User loggedUser) {
        User userToUpdate = userServiceFinder.getById(updateData.getId());

        if (!loggedUser.hasRole(ROLE_ADMIN))
            userValidationService.validateCorrectPassword(updateData.getPassword(), userToUpdate.getPassword());

        userValidationService.validateUniqueEmail(updateData.getEmail(), userToUpdate.getId());
        User updatedUser = mapper.toUser(updateData, userToUpdate);
        return userRepository.save(updatedUser);
    }

    @Override
    public void deactivate(DeleteUserDto userDto, User loggedUser) {
        User user = userServiceFinder.getById(userDto.getId());

        if (!loggedUser.hasRole(ROLE_ADMIN))
            userValidationService.validateCorrectPassword(userDto.getPassword(), user.getPassword());

        user.deactivate();
        userRepository.save(user);
    }

    @Override
    public void activate(int id, User loggedUser) {
        User user = userServiceFinder.getById(id);
        userValidationService.validateAdmin(loggedUser);

        user.activate();
        userRepository.save(user);
    }

    @Override
    public void activateWithToken(User user, ConfirmationToken token) {
        userValidationService.validateToken(user, token);
        user.activate();
        userRepository.save(user);
    }

    @Override
    public void addServiceToFavourite(User user, AppService appService) {
        user.addServiceToFavourite(appService);
    }

    @Override
    public void removeServiceFromFav(User user, AppService appService) {
        user.removeServiceFromFavourite(appService);
    }
}
