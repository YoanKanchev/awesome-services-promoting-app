package com.example.aservpro.services;

import com.example.aservpro.models.AppService;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.AppServiceFilterParameterDto;
import com.example.aservpro.models.dtobjects.CreateAppServiceDto;

import java.util.List;

public interface AppServiceService {

    List<AppService> getAll();

    AppService getById(int id);

    AppService create(CreateAppServiceDto serviceDto, User loggedUser);

    AppService update(AppService service, User loggedUser);

    void delete(AppService service, User loggedUser);

    List<AppService> filter(AppServiceFilterParameterDto appServiceFilterParameterDto);

    AppService incrementViews(AppService appService);
}
