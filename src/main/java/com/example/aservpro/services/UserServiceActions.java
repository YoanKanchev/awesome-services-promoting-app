package com.example.aservpro.services;


import com.example.aservpro.models.AppService;
import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.users.CreateUserDto;
import com.example.aservpro.models.dtobjects.users.DeleteUserDto;
import com.example.aservpro.models.dtobjects.users.UpdateUserDto;

public interface UserServiceActions {

    User create(CreateUserDto user);

    User update(UpdateUserDto user, User loggedUser);

    void deactivate(DeleteUserDto userDto, User loggedUser);

    void activate(int id, User loggedUser);

    void activateWithToken(User user, ConfirmationToken token);

    void addServiceToFavourite(User user, AppService appService);

    void removeServiceFromFav(User user, AppService appService);

}
