package com.example.aservpro.services;

import com.example.aservpro.models.AppService;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.users.UserFilterParametersDto;

import java.util.List;


public interface UserServiceFinder{
    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    List<User> filter(UserFilterParametersDto userFilterParametersDto);

    List<AppService> getFavouriteServices(User loggedUser);

}
