package com.example.aservpro.services;

import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;

import java.util.List;

public interface ConfirmationTokenService {

    List<ConfirmationToken> getAll();

    ConfirmationToken getByTokenValue(String tokenValue);

    ConfirmationToken getById(int id);

    void delete(int id, User loggedUser);

    ConfirmationToken newToken(User user);
}
