package com.example.aservpro.services;

import com.example.aservpro.exceptions.EntityNotFoundException;
import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;
import com.example.aservpro.repositories.ConfirmationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static com.example.aservpro.helpers.GlobalConstants.NOT_FOUND;
import static com.example.aservpro.helpers.GlobalConstants.TOKEN_ENTITY;

@Service
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {

    public static final int EXPIRATION_TIME_IN_HOURS = 1;
    private final ConfirmationTokenRepository tokenRepository;

    @Autowired
    public ConfirmationTokenServiceImpl(ConfirmationTokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public List<ConfirmationToken> getAll() {
        return tokenRepository.findAll();
    }

    @Override
    public ConfirmationToken getByTokenValue(String tokenValue) {
        return tokenRepository.findByTokenValue(tokenValue)
                .orElseThrow(() -> new EntityNotFoundException(TOKEN_ENTITY + NOT_FOUND));
    }

    @Override
    public ConfirmationToken getById(int id) {
        return tokenRepository.getOne(id);
    }

    @Override
    public void delete(int id, User loggedUser) {
        ConfirmationToken token = getById(id);
        tokenRepository.delete(token);
    }

    @Override
    public ConfirmationToken newToken(User user) {
        ConfirmationToken token = new ConfirmationToken(generateTokenValue(), user, EXPIRATION_TIME_IN_HOURS);
        return tokenRepository.save(token);
    }

    private String generateTokenValue() {
        String token = UUID.randomUUID().toString();
        while (tokenRepository.findByTokenValue(token).isPresent()) {
            token = UUID.randomUUID().toString();
        }
        return token;
    }
}
