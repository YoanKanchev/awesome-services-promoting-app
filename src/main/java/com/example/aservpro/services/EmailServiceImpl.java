package com.example.aservpro.services;

import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.EmailDtoHtml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.util.Map;

import static com.example.aservpro.helpers.GlobalConstants.*;

@PropertySource("classpath:application.properties")
@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender mailSender;
    private final String from;
    private final SpringTemplateEngine templateEngine;

    @Autowired
    public EmailServiceImpl(JavaMailSender mailSender, Environment env, SpringTemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.from = env.getProperty("spring.mail.username");
        this.templateEngine = templateEngine;
    }

    @Override
    @Async
    public void sendEmail(String to, String subject, String message) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(to);
        email.setSubject(subject);
        email.setFrom(from);
        email.setText(message);
        mailSender.send(email);
    }

    @Override
    public void sendHtmlEmailFromTemplate(EmailDtoHtml emailDtoHtml) throws MessagingException {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(emailDtoHtml.getTemplateModel());
        String htmlBody = templateEngine.process(emailDtoHtml.getTemplate(), thymeleafContext);
        sendHtmlMessage(emailDtoHtml.getTo(), emailDtoHtml.getSubject(), htmlBody);
    }

    @Override
    public void sendRegistrationConfirmationEmail(ConfirmationToken token, String url) throws MessagingException {
        String fullUrl = url + PATH_FOR_REGISTRATION_CONFIRMATION + token.getTokenValue();
        EmailDtoHtml emailDto = generateEmailDtoHtml(token, fullUrl, SUBJECT_FOR_REGISTRATION_CONFIRMATION,
                TEMPLATE_FOR_REGISTRATION_CONFIRMATION);
        sendHtmlEmailFromTemplate(emailDto);
    }

    @Override
    public void sendPasswordRecoveryMail(ConfirmationToken token, String url) throws MessagingException {
        String fullUrl = url + PATH_FOR_PASSWORD_RECOVERY_HANDLER + token.getTokenValue();
        EmailDtoHtml emailDto = generateEmailDtoHtml(token, fullUrl, SUBJECT_FOR_PASSWORD_RECOVERY, TEMPLATE_FOR_PASSWORD_RECOVERY);
        sendHtmlEmailFromTemplate(emailDto);
    }

    private void sendHtmlMessage(String to, String subject, String htmlBody) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, UTF_8);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(htmlBody, true);
        mailSender.send(message);
    }

    public EmailDtoHtml generateEmailDtoHtml(ConfirmationToken token, String url, String subject, String template) {
        User user = token.getUser();
        String username = user.getFirstName() + " " + user.getLastName();
        String userEmail = user.getEmail();
        Map<String, Object> htmlParameters = Map.of("username", username, "url", url);
        return new EmailDtoHtml(userEmail, subject, template, htmlParameters);
    }

}
