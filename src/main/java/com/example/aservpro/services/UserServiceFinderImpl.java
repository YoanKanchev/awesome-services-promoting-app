package com.example.aservpro.services;

import com.example.aservpro.exceptions.EntityNotFoundException;
import com.example.aservpro.helpers.FilterBuilder;
import com.example.aservpro.helpers.specifications.FilterSpecification;
import com.example.aservpro.models.AppService;
import com.example.aservpro.models.User;
import com.example.aservpro.models.dtobjects.users.UserFilterParametersDto;
import com.example.aservpro.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.aservpro.helpers.GlobalConstants.*;

@Service
public class UserServiceFinderImpl implements UserServiceFinder {

    private final UserRepository userRepository;
    private final FilterBuilder filterBuilder;

    @Autowired
    public UserServiceFinderImpl(UserRepository userRepository, FilterBuilder filterBuilder) {
        this.userRepository = userRepository;
        this.filterBuilder = filterBuilder;

    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER_ENTITY + WITH_ID + NOT_FOUND, id)));
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER_ENTITY + WITH_EMAIL + NOT_FOUND, email)));
    }

    @Override
    public List<User> filter(UserFilterParametersDto userFilterParametersDto) {
        FilterSpecification<User> userFilterSpecification = filterBuilder.createSpecification(userFilterParametersDto);
        Sort sort = filterBuilder.getSortOrders(userFilterParametersDto);
        return userRepository.findAll(userFilterSpecification, sort);
    }

    @Override
    public List<AppService> getFavouriteServices(User loggedUser) {
        return loggedUser.getFavouriteServices();
    }
}
