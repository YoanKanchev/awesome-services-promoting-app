package com.example.aservpro.services;

import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.dtobjects.EmailDtoHtml;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public interface EmailService {

    void sendEmail(String to, String subject, String message);

    void sendHtmlEmailFromTemplate(EmailDtoHtml emailDtoHtml) throws MessagingException;

    void sendRegistrationConfirmationEmail(ConfirmationToken token, String url) throws MessagingException;

    void sendPasswordRecoveryMail(ConfirmationToken token, String url) throws MessagingException;

}
