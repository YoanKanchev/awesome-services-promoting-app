package com.example.aservpro.services.validation;

import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;

public interface UserValidationService {

    void roleAuthorisationValidation(User loggedUser, String role);

    void validateUniqueEmail(String email);

    void validateUniqueEmail(String email, Integer userId);

    void validateCorrectPassword(String password, String realPassword);

    void validateAdmin(User loggedUser);

    void validateToken(User user, ConfirmationToken token);
}
