package com.example.aservpro.services.validation;


import com.example.aservpro.enums.Role;
import com.example.aservpro.exceptions.AuthorisationException;
import com.example.aservpro.exceptions.DuplicateEntityException;
import com.example.aservpro.helpers.GlobalConstants;
import com.example.aservpro.models.ConfirmationToken;
import com.example.aservpro.models.User;
import com.example.aservpro.models.UserRole;
import com.example.aservpro.repositories.UserRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.example.aservpro.helpers.GlobalConstants.*;

@Component
public class UserValidationServiceImpl implements UserValidationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserValidationServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void roleAuthorisationValidation(User loggedUser, String role) {
        if ((!loggedUser.isEnabled())
                || !loggedUser.hasRole(Role.valueOf(role))) {
            throw new AuthorisationException(GlobalConstants.YOU_ARE_UNAUTHORISED);
        }
    }

    @Override
    public void validateUniqueEmail(String email) {
        if (userRepository.existsByEmail(email)) {
            throw new DuplicateEntityException(
                    String.format(USER_ENTITY + WITH_EMAIL + ALREADY_EXISTS, email));
        }
    }

    @Override
    public void validateUniqueEmail(String email, Integer userId) {
        Optional<User> optionalUser = userRepository.findByEmail(email);
        if (optionalUser.isPresent()) {
            User userToCheck = optionalUser.get();
            if (userToCheck.getId() != userId) {
                throw new DuplicateEntityException(
                        String.format(USER_ENTITY + WITH_EMAIL + ALREADY_EXISTS, email));
            }
        }
    }

    @Override
    public void validateCorrectPassword(String password, String realPassword) {
        boolean correctPassword = !Strings.isEmpty(password) && passwordEncoder.matches(password, realPassword);
        if (!correctPassword)
            throw new AuthorisationException(WRONG_CONFIRMATION_PASSWORD);
    }

    @Override
    public void validateAdmin(User loggedUser) {
        boolean loggedUserIsAdmin = loggedUser.hasRole(Role.ROLE_ADMIN);
        if (!loggedUserIsAdmin)
            throw new AuthorisationException(YOU_ARE_UNAUTHORISED);
    }

    @Override
    public void validateToken(User user, ConfirmationToken token) {
        boolean isCreator = token.getUser().equals(user);
        if (!isCreator)
            throw new AuthorisationException(YOU_ARE_UNAUTHORISED);
    }

}
