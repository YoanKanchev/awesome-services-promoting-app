package com.example.aservpro.services.validation;

import com.example.aservpro.models.User;
import com.example.aservpro.models.interfaces.EntityWithCreator;

public interface BaseValidationsService {

    void adminORCreatorCheck(EntityWithCreator entity, User loggedUser);

    <T extends Enum<T>> void validateEnum(String toValidate, Class<T> enumerator);
}
