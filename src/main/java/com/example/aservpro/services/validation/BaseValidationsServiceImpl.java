package com.example.aservpro.services.validation;


import com.example.aservpro.exceptions.AuthorisationException;
import com.example.aservpro.exceptions.EntityNotFoundException;
import com.example.aservpro.models.User;
import com.example.aservpro.models.interfaces.EntityWithCreator;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static com.example.aservpro.enums.Role.ROLE_ADMIN;
import static com.example.aservpro.helpers.GlobalConstants.INVALID_ROLE;
import static com.example.aservpro.helpers.GlobalConstants.YOU_ARE_UNAUTHORISED;


@Service
public class BaseValidationsServiceImpl implements BaseValidationsService {

    @Override
    public void adminORCreatorCheck(EntityWithCreator entity, User loggedUser) {
        if ((!loggedUser.isEnabled())
                || (!loggedUser.hasRole(ROLE_ADMIN)
                && !loggedUser.equals(entity.getCreator()))) {
            throw new AuthorisationException(YOU_ARE_UNAUTHORISED);
        }
    }

    @Override
    public <T extends Enum<T>> void validateEnum(String toValidate, Class<T> enumerator) {
        Arrays.stream(enumerator.getEnumConstants()).filter(x -> x.toString().equals(toValidate)).findAny()
                .orElseThrow(() -> new EntityNotFoundException(INVALID_ROLE));
    }
}
