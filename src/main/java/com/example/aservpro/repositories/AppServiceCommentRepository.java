package com.example.aservpro.repositories;

import com.example.aservpro.models.AppServiceComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppServiceCommentRepository extends JpaRepository<AppServiceComment, Integer> {

}
