package com.example.aservpro.repositories;

import com.example.aservpro.models.AppService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AppServiceRepository extends JpaRepository<AppService, Integer>, JpaSpecificationExecutor<AppService> {
}
