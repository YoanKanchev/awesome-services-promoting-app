package com.example.aservpro.repositories;

import com.example.aservpro.models.AppServiceRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AppServiceRatingRepository extends JpaRepository<AppServiceRating, Integer> {
//
//		Set<AppServiceRating> findAllByCreator_Id(int creatorId);
//
//		Set<AppServiceRating> findAllByService_Id(int serviceId);

}
