//package com.example.aservpro.service;
//
//import com.example.aservpro.exceptions.EntityNotFoundException;
//import com.example.aservpro.helpers.Mapper;
//import com.example.aservpro.helpers.ValidationHelper;
//import com.example.aservpro.models.User;
//import com.example.aservpro.models.UserAuthentication;
//import com.example.aservpro.models.dtobjects.users.CreateUserDto;
//import com.example.aservpro.repositories.UserRepository;
//import com.example.aservpro.services.UserServiceImpl;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//
//import static org.mockito.Mockito.*;
//import static org.mockito.Mockito.times;
//
//@ExtendWith(MockitoExtension.class)
//public class UserServiceTest {
//    @InjectMocks
//    UserServiceImpl userService;
//
//    @Mock
//    Mapper mapper;
//
//    @Mock
//    UserRepository userRepository;
//
//    @Mock
//    ValidationHelper validationHelper;
//
//    @Mock
//    PasswordEncoder passwordEncoder;
//
//    @Test
//    public void getAll_Should_Pass_Finding_All_Users() {
//        //Arrange
//        when(userRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new User(), new User())));
//        //Act
//        //Assert
//        Assertions.assertEquals(2, userService.getAll().size());
//    }
//
//    @Test
//    public void getById_Should_Pass_When_Passed_Correct_Id() {
//        //Arrange
//        int id = Mockito.anyInt();
//        User test = new User();
//        test.setId(id);
//        when(userRepository.findById(id)).thenReturn(java.util.Optional.of(test));
//
//        //Act
//        User user = userService.getById(id);
//
//        //Assert
//        Assertions.assertEquals(id, user.getId());
//    }
//
//    @Test
//    public void getById_Should_Fail_When_Passed_NotExisting_Id() {
//        //Arrange
//        int id = Mockito.anyInt();
//        when(userRepository.findById(id)).thenReturn(java.util.Optional.empty());
//        //Act
//        //Assert
//        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getById(id));
//    }
//
//    @Test
//    public void getByUsername_Should_Pass_When_Passed_Correct_Username() {
//        //Arrange
//        String username = Mockito.anyString();
//        User test = new User();
//        UserAuthentication userAuthentication = new UserAuthentication();
//        userAuthentication.setUsername(username);
//        test.setUserAuthentication(userAuthentication);
//        when(userRepository.findByUserAuthentication_Username(username)).thenReturn(java.util.Optional.of(test));
//
//        //Act
//        User user = userService.getByUsername(username);
//
//        //Assert
//        Assertions.assertEquals(username, user.getUsername());
//    }
//
//    @Test
//    public void getByUsername_Should_Fail_When_Passed_NotExisting_Username() {
//        //Arrange
//        String username = Mockito.anyString();
//        when(userRepository.findByUserAuthentication_Username(username)).thenReturn(java.util.Optional.empty());
//        //Act
//        //Assert
//        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getByUsername(username));
//    }
//
//    @Test
//    public void create_Should_Succeed_With_Correct_Input() {
//        //Arrange
//        CreateUserDto userDto = getRandomUserDto();
//        UserAuthentication userAuthentication = getRandomUserAuthority(userDto);
//        User user = getRandomUser(userDto, userAuthentication);
//
//        doNothing().when(validationHelper).usernameDuplicationValidator(userDto.getUsername());
//        when(passwordEncoder.encode(userDto.getPassword())).thenReturn(userDto.getPassword());
//        when(mapper.toAuthentication(userDto)).thenReturn(userAuthentication);
//        when(mapper.toUser(userDto, userAuthentication)).thenReturn(user);
//        when(userRepository.save(user)).thenReturn(user);
//        //Act
//        userService.create(userDto);
//        //Assert
//        verify(userRepository, times(1)).save(user);
//    }
//
//    private User getRandomUser(CreateUserDto userDto, UserAuthentication userAuthentication) {
//        User user = new User();
//        user.setId(userDto.getId());
//        user.setEmail(userDto.getEmail());
//        user.setFirstName(userDto.getFirstName());
//        user.setLastName(userDto.getLastName());
//        user.setUserAuthentication(userAuthentication);
//        return user;
//    }
//
//    private UserAuthentication getRandomUserAuthority(CreateUserDto userDto) {
//        UserAuthentication userAuthentication = new UserAuthentication();
//        userAuthentication.setUsername(userDto.getUsername());
//        userAuthentication.setPassword(userDto.getPassword());
//        return userAuthentication;
//    }
//
//    private CreateUserDto getRandomUserDto() {
//        String username = new String(new char[4]);
//        String firstname = new String(new char[4]);
//        String lastname = new String(new char[4]);
//        String password = new String(new char[4]);
//        String email = "test@abv.bg";
//        String role = "ROLE_USER";
//        return new CreateUserDto(1, username, firstname, lastname, password, password, email, role);
//    }
//
//
//}
